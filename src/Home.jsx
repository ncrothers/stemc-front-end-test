
import React, { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card';

import BackendApi from './backendApi';

export const Home = props => {

    const [label, setLabel] = useState("");
    const [scoreMax, setScoreMax] = useState("");
    const [page, setPage] = useState(1);
    const [isLoading, setIsLoading] = useState(true);
    const [lineItems, setLineItems] = useState([]);

    const backendApi = new BackendApi();

    const onCreateAssignment = () => {
        if (backendApi.createAssignment(label, scoreMax)) {
            setLabel("");
            setScoreMax("");
            loadLineItems();
        }
    }

    const loadLineItems = () => {
        setPage(page + 1);
        setIsLoading(true);
    };

    // Loads from API
    useEffect(() => {
        backendApi.getLineItems()
            .then(response => {
                debugger;
                setLineItems(response.response);
                setIsLoading(false);
            })
            .catch(e => {
                console.error(e);
                alert(e);
            });
    }, [page, backendApi]);

    return (
        <div className="container">
            <Form>
                <Form.Group>
                    <Form.Label htmlFor="label">Assignment Title</Form.Label>
                    <Form.Control
                        id="label"
                        type="text"
                        value={ label }
                        onChange={ e => setLabel(e.target.value) } 
                        placeholder="Assignment 1 in Minecraft"/>
                </Form.Group>
                <Form.Group>
                    <Form.Label htmlFor="scoreMax">Maximum Score</Form.Label>
                    <Form.Control
                        id="scoreMax"
                        type="number"
                        value={ scoreMax }
                        onChange={ e => setScoreMax(e.target.value) } 
                        placeholder="100"/>
                </Form.Group>
                <Button 
                    variant="primary" 
                    block
                    onClick={ () => onCreateAssignment() }>
                    Create Assignment
                </Button>
            </Form>
            <hr />
            { isLoading && <p>Loading line items...</p> }
            {
                lineItems.map(x => 
                    <Card>
                        <Card.Title>{ x.label }</Card.Title>
                        <Card.Subtitle>{ x.scoreMaximum }</Card.Subtitle>
                    </Card>)
            }
        </div>
    )
}

export default Home;