
import axios from 'axios';

import { backendHostname } from './config';

export class BackendApi {

    createAssignment = async (label, scoreMaximum) => {
        const config = {
            headers: {
                Authorization: "SmQ0aUNXdWh3ZgpNOFhLTnZpQlYwCmxzZzZWRE1tQVMKSWdwUnRjcEhvWgpubU1Qb3hEMWhP"
            }
        }
        try {
            await axios.post(backendHostname + '/canvas/createAssignment', { label, scoreMaximum }, config);
            return true;
        }
        catch (e) {
            console.error(e);
            alert(e);
            return false;
        }
    }

    getLineItems = async () => {
        const config = {
            headers: {
                Authorization: "SmQ0aUNXdWh3ZgpNOFhLTnZpQlYwCmxzZzZWRE1tQVMKSWdwUnRjcEhvWgpubU1Qb3hEMWhP"
            }
        }
        try {
            let response = await axios.get(backendHostname + '/canvas/getLineItems', config);
            return response.data;
        }
        catch (e) {
            console.error(e);
            alert(e);
            return [];
        }
    }
}

export default BackendApi;